#define DEBUG 1
// #define TEST 1

// Output
const int RED_PIN = 9;   // Red LED,   connected to digital pin 9
const int GREEN_PIN = 10;  // Green LED, connected to digital pin 10
const int BLUE_PIN = 11;  // Blue LED,  connected to digital pin 11

enum {
  MODE_RUNNING = 0x01,
  MODE_HOLD = 0x02,
  MODE_ANIMATION_TICK = 0x04,
};

// used globally, set by `delayAndListen`
byte delayMode = MODE_RUNNING;

// sequence:
typedef struct SequenceItem {
  int transition; // in seconds
  int duration; // in seconds including transition
  byte r; // 0 - 255
  byte g; // 0 - 255
  byte b; // 0 - 255
} SequenceItem;

int const SEQ_COUNT = 28
#if TEST == 1
+ 5
#endif
; // +1 for the fade to black and +1 for back to white then back to black and exit.
SequenceItem sequence[SEQ_COUNT] = {
#if TEST == 1
// color test
(SequenceItem) {1,1,
  255,0,0},
(SequenceItem) {1,1,
  0,255,0},
(SequenceItem) {1,1,
  0,0,255},
(SequenceItem) {1,1,
  255,0,255},
(SequenceItem) {1,1,
  255,255,0},
#endif
(SequenceItem) {1,2,
  255,201,14},
(SequenceItem) {15,173,
  134,44,0},
(SequenceItem) {23,71,
  255,128,0},
(SequenceItem) {7,23,
  128,64,64},
(SequenceItem) {13,40,
  0,255,255},
(SequenceItem) {28,86,
  233,127,67},
(SequenceItem) {12,37,
  94,132,177},
(SequenceItem) {14,44,
  0,0,127},
(SequenceItem) {29,89,
  0,127,127},
(SequenceItem) {14,43,
  0,0,64},
(SequenceItem) {23,70,
  255,0,128},
(SequenceItem) {15,47,
  128,0,64},
(SequenceItem) {17,51,
  128,64,0},
(SequenceItem) {12,38,
  128,0,128},
(SequenceItem) {8,26,
  128,0,64},
(SequenceItem) {4,4,
  128,128,192},
(SequenceItem) {4,13,
  0,64,64},
(SequenceItem) {9,29,
  0,64,128},
(SequenceItem) {28,85,
  255,128,64},
(SequenceItem) {33,100,
  64,0,64},
(SequenceItem) {20,60,
  0,0,160},
(SequenceItem) {30,153,
  0,128,128},
(SequenceItem) {28,85,
  255,0,128},
(SequenceItem) {14,43,
  224,49,39},
(SequenceItem) {35,106,
  0,0,64},
(SequenceItem) {1,60,
  255,255,255},
(SequenceItem) {1,1,
  0,0,0}
};

int wait = 10;      // 10ms internal crossFade delay; increase for slower fades
int hold = 0;       // Optional hold when a color is complete, before the next crossFade

// frames per second
int FPS = 60;
int tick = 1000/FPS;

// used for timing -- so we can listen to switches without delay
int delayStart = 0;
int delayEnd = 0;

// used to easing
int startTime = 0;
int currentTime = 0;
int duration = 0;

// trans the current
int sequenceIndex = 0;
SequenceItem previousColor = (SequenceItem) {1, 1, 0,0,0 };
SequenceItem currentColor;

// Set up the LED outputs
void setup()
{
  pinMode(RED_PIN, OUTPUT);   // sets the pins as output
  pinMode(GREEN_PIN, OUTPUT);   
  pinMode(BLUE_PIN, OUTPUT); 

  if (DEBUG) {           // If we want to see values for debugging...
    Serial.begin(9600);  // ...set up the serial ouput 
    Serial.println("Sequence starting.");
  }
}

// Main program: list the order of crossfades
void loop()
{

  if (sequenceIndex < SEQ_COUNT) {
    currentColor = sequence[sequenceIndex];
    hold = (currentColor.duration - currentColor.transition) * 1000;
    // quad Out at 60 fps (16ms delay);
    transitionTo(currentColor);
    if (DEBUG) {
      Serial.print("Holding for ");
      Serial.print(currentColor.duration - currentColor.transition);
      Serial.println(" seconds");
    }
#ifndef TEST
    delayAndListen(hold, MODE_HOLD);
#endif
    if (DEBUG) {
      Serial.println("done.");
    }
    previousColor = currentColor;
    sequenceIndex++;
  }
}

// delay, but also do io stuff
void delayAndListen(int delayFor, int mode) {
  delayStart = millis();
  delayEnd = delayStart + delayFor;
  int now = millis();
  while (now < delayEnd) {
    // do things with peripherals

    now = millis();
    // debug
    // if (DEBUG && mode & MODE_HOLD) {
    //   Serial.print("Been holding for ");
    //   Serial.print(now-delayStart);
    //   Serial.println(" milliseconds.");
    // }
  }
  mode = MODE_RUNNING;
}

struct SequenceItem mixColors(struct SequenceItem *color1, struct SequenceItem *color2, float p) {
    float w = p * 2.0f - 1.0f;
    float w1 = (w + 1.0f) / 2.0f;
    float w2 = 1.0f - w1;

    struct SequenceItem rgb = (SequenceItem) {
      transition: 0,
      duration: 0,
      r: floor(((float)color2->r * w1) + ((float)color1->r * w2)),
      g: floor(((float)color2->g * w1) + ((float)color1->g * w2)),
      b: floor(((float)color2->b * w1) + ((float)color1->b * w2))
    };

    return rgb;
};

void transitionTo(struct SequenceItem color) {
  startTime = 0;
  currentTime = 0;
  duration = color.transition*1000;
#ifdef TEST
  duration = 1000;
#endif
  // int rDiff = color.r - previousColor.r;
  // int gDiff = color.g - previousColor.g;
  // int bDiff = color.b - previousColor.b;
  // int rDiff = previousColor.r - color.r;
  // int gDiff = previousColor.g - color.g;
  // int bDiff = previousColor.b - color.b;
  while (currentTime < duration) {
    float p = (float)currentTime/(float)duration;
    p = quadraticOut(p);
    
    SequenceItem rgb = mixColors(&previousColor, &color, p);

    analogWrite(RED_PIN, rgb.r);   // Write current values to LED pins
    analogWrite(GREEN_PIN, rgb.g);
    analogWrite(BLUE_PIN, rgb.b);

    currentTime += tick;
    // so we can listen to switches:
    delayAndListen(16, MODE_ANIMATION_TICK);
    // if (DEBUG && currentTime % 480 == 0) {
    if (DEBUG) {
      Serial.print(rgb.r);
      Serial.print(",");
      Serial.print(rgb.g);
      Serial.print(",");  
      Serial.println(rgb.b); 
    }
  }

  return;
}

// quadratic easing
float quadraticOut(float p) {
  return -(p * (p - 2.0f));
}
