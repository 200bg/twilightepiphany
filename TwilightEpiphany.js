const DEBUG = 1
const TEST = 1

// Output
const RED_PIN = 9;   // Red LED,   connected to digital pin 9
const GREEN_PIN = 10;  // Green LED, connected to digital pin 10
const BLUE_PIN = 11;  // Blue LED,  connected to digital pin 11


const MODE_RUNNING = 0x01;
const MODE_HOLD = 0x02;
const MODE_ANIMATION_TICK = 0x04;

// used globally, set by `delayAndListen`
var delayMode = MODE_RUNNING;

// sequence:
// typedef struct SequenceItem {
//   int transition; // in seconds
//   int duration; // in seconds including transition
//   byte r; // 0 - 255
//   byte g; // 0 - 255
//   byte b; // 0 - 255
// } SequenceItem;

var sequence = [
  // color test
  // {transition: 1, duration:1, r:255, g:0,  b:0},
  // {transition: 1, duration:1, r:0,   g:25, b:5},
  // {transition: 1, duration:1, r:0,   g:0,  b:255},
  // {transition: 1, duration:1, r:255, g:0,  b:255},
  // {transition: 1, duration:1, r:255, g:25, b:5},

  {transition: 1, duration: 25, r:255, g:201, b:14},
  {transition: 20, duration: 173, r:134, g:44, b:0},
  {transition: 23, duration: 71, r:255, g:128, b:0},
  {transition: 7, duration: 23, r:128, g:64, b:64},
  {transition: 13, duration: 40, r:0, g:255, b:255},
  {transition: 28, duration: 86, r:233, g:127, b:67},
  {transition: 12, duration: 37, r:94, g:132, b:177},
  {transition: 14, duration: 44, r:0, g:0, b:127},
  {transition: 29, duration: 89, r:0, g:127, b:127},
  {transition: 14, duration: 43, r:0, g:0, b:64},
  {transition: 23, duration: 70, r:255, g:0, b:128},
  {transition: 15, duration: 47, r:128, g:0, b:64},
  {transition: 17, duration: 51, r:128, g:64, b:0},
  {transition: 12, duration: 38, r:128, g:0, b:128},
  {transition: 8, duration: 26, r:128, g:0, b:64},
  {transition: 4, duration: 4, r:128, g:128, b:192},
  {transition: 4, duration: 13, r:0, g:64, b:64},
  {transition: 9, duration: 29, r:0, g:64, b:128},
  {transition: 28, duration: 85, r:255, g:128, b:64},
  {transition: 33, duration: 100, r:64, g:0, b:64},
  {transition: 20, duration: 60, r:0, g:0, b:160},
  {transition: 30, duration: 153, r:0, g:128, b:128},
  {transition: 28, duration: 85, r:255, g:0, b:128},
  {transition: 14, duration: 43, r:224, g:49, b:39},
  {transition: 35, duration: 106, r:0, g:0, b:64},
  {transition: 1, duration: 60, r:255, g:255, b:255},
  {transition: 1, duration: 1, r:0, g:0, b:0}
];

var wait = 10;      // 10ms internal crossFade delay; increase for slower fades
var hold = 0;       // Optional hold when a color is complete, before the next crossFade

// frames per second
const FPS = 60;
var tick = 1000/FPS;

// used for timing -- so we can listen to switches without delay
var delayStart = 0;
var delayEnd = 0;

// used to easing
var startTime = 0;
var currentTime = 0;
var duration = 0;

// trans the current
var sequenceIndex = 0;
var previousColor = {transition:1, duration:1, r:0, g:0, b:0};
var currentColor;
var toColor;

function millis() {
  return new Date().valueOf();
}

// Main program: list the order of crossfades
function loop() {

  if (sequenceIndex < sequence.length) {
  // if (sequenceIndex < 2) {
    currentColor = sequence[sequenceIndex];
    hold = (currentColor.duration - currentColor.transition) * 1000;;
    // quad Out at 60 fps (16ms delay);
    transitionTo(currentColor);
    if (DEBUG) {
      console.log("Holding for " + (currentColor.duration - currentColor.transition) + " seconds");
    }
    if (!TEST) {
      delayAndListen(hold, MODE_HOLD);
    }
    if (DEBUG) {
      console.log("done.");
    }
    sequenceIndex++;
  }
}

function timeline(c) {
  var ctx,
      currentSecond = 0,
      totalSeconds = 0,
      transitionalColor,
      transitionalColorString,
      i,s,j,p;

  // resize the canvas
  for (i = sequence.length - 1; i >= 0; i--) {
    s = sequence[i];
    totalSeconds += s.duration;
  };

  c.width = totalSeconds;
  ctx = c.getContext('2d');

  // draw things
  for (i = 0; i < sequence.length; i++) {
    toColor = sequence[i];
    for (j = 0; j < toColor.duration; j++) {
      if (j < toColor.transition) {
        p = j/toColor.transition;
        p = quadraticOut(p);
        transitionalColor = mixColors(previousColor, toColor, p);
      } else {
        transitionalColor = mixColors(previousColor, toColor, 1.0);
      }
      transitionalColorString = 'rgb(' + transitionalColor.r + ',' + transitionalColor.g + ',' + transitionalColor.b + ')';
      console.log(transitionalColorString);
      ctx.fillStyle = transitionalColorString;
      ctx.fillRect(currentSecond, 0, 1, c.height);
      currentSecond++;
    };
    previousColor = toColor;
  };
}

// delay, but also do io stuff
function delayAndListen(delayFor, mode) {
  delayStart = millis();
  delayEnd = delayStart + delayFor;
  var now = millis();
  // while (now < delayEnd) {
  //   // do things with peripherals

  //   now = millis();
  //   // debug
  //   // if (DEBUG && mode & MODE_HOLD) {
  //   //   Serial.print("Been holding for ");
  //   //   Serial.print(now-delayStart);
  //   //   Serial.println(" milliseconds.");
  //   // }
  // }
  mode = MODE_RUNNING;
}

function onAnimationFrame (t) {
  var p = currentTime/duration;
  p = quadraticOut(p);

  var transitionalColor = mixColors(previousColor, toColor, p);
  document.body.style.backgroundColor = 'rgb(' + transitionalColor.r + ',' + transitionalColor.g + ',' + transitionalColor.b + ')';

  currentTime += tick;
  // so we can listen to switches:
  delayAndListen(16, MODE_ANIMATION_TICK);

  // if (DEBUG && currentTime % 480 == 0) {
  if (DEBUG) {
    console.log('rgb(' + transitionalColor.r + ',' + transitionalColor.g + ',' + transitionalColor.b + ')');
  }

  if (currentTime < duration) {
    requestAnimationFrame(onAnimationFrame);
  } else {
    previousColor = currentColor;
    loop();
  }
}

function mixColors(color1, color2, p) {
    var w = p * 2 - 1;
    var w1 = (w + 1) / 2;

    var w2 = 1 - w1;

    var rgb = {
        r: Math.floor((color2.r * w1) + (color1.r * w2)),
        g: Math.floor((color2.g * w1) + (color1.g * w2)),
        b: Math.floor((color2.b * w1) + (color1.b * w2))
    };

    return rgb;
};

function transitionTo(color) {
  startTime = 0;
  currentTime = 0;
  duration = color.transition*1000;
  if (TEST) {
    duration = 1000;
  }
  toColor = color;

  requestAnimationFrame(onAnimationFrame);
  return;
}

// quadratic easing
function quadraticOut(p) {
  return -(p * (p - 2.0));
}
